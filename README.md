# CS 373: Software Engineering Collatz Repo

* Name: Curtis Nichols

* EID: cjn857

* GitLab ID: curtis_nichols

* HackerRank ID: nichols_curtis_j

* Git SHA: 3c233114b91a9355710e9c8329db3f77323dd74d

* GitLab Pipelines: https://gitlab.com/curtis_nichols/cs373-collatz/-/pipelines

* Estimated completion time: 8

* Actual completion time: 6

* Comments: I referenced my work on the OOP Collatz project while working
	    on this.
