#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import (
    collatz_read,
    collatz_eval,
    collatz_print,
    collatz_solve,
    collatz_cycle_length,
    LCACHE_SIZE,
)

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        lazy_cache = [0] * LCACHE_SIZE
        v = collatz_eval((1, 10), lazy_cache)
        self.assertEqual(v, (1, 10, 20))

    def test_eval_2(self):
        lazy_cache = [0] * LCACHE_SIZE
        v = collatz_eval((100, 200), lazy_cache)
        self.assertEqual(v, (100, 200, 125))

    def test_eval_3(self):
        lazy_cache = [0] * LCACHE_SIZE
        v = collatz_eval((201, 210), lazy_cache)
        self.assertEqual(v, (201, 210, 89))

    def test_eval_4(self):
        lazy_cache = [0] * LCACHE_SIZE
        v = collatz_eval((900, 1000), lazy_cache)
        self.assertEqual(v, (900, 1000, 174))

    # -----
    # print
    # -----

    def test_print(self):
        sout = StringIO()
        collatz_print(sout, (1, 10, 20))
        self.assertEqual(sout.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        lazy_cache = [0] * LCACHE_SIZE
        sin = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        sout = StringIO()
        collatz_solve(sin, sout)
        self.assertEqual(
            sout.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )

    # same as above, but reversed
    def test_solve_2(self):
        lazy_cache = [0] * LCACHE_SIZE
        sin = StringIO("10 1\n200 100\n210 201\n1000 900\n")
        sout = StringIO()
        collatz_solve(sin, sout)
        self.assertEqual(
            sout.getvalue(), "10 1 20\n200 100 125\n210 201 89\n1000 900 174\n"
        )

    # test range of 1
    def test_solve_3(self):
        lazy_cache = [0] * LCACHE_SIZE
        sin = StringIO("3 3\n")
        sout = StringIO()
        collatz_solve(sin, sout)
        self.assertEqual(sout.getvalue(), "3 3 8\n")

    # test big range between big numbers (enough to use meta cache)
    def test_solve_4(self):
        lazy_cache = [0] * LCACHE_SIZE
        sin = StringIO("555234 683258\n")
        sout = StringIO()
        collatz_solve(sin, sout)
        self.assertEqual(sout.getvalue(), "555234 683258 509\n")

    # -----
    # cycle length
    # -----
    def test_cl_1(self):
        lazy_cache = [0] * LCACHE_SIZE
        v = collatz_cycle_length(3, lazy_cache)
        self.assertEqual(v, 8)


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
