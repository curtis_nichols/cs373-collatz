from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, collatz_cycle_length, LCACHE_SIZE, MCACHE_INTERVAL
from typing import IO, Tuple, Union, List
from random import random


def max_cl_range(b: int, e: int, lc: List[int]) -> int:
	t1, t2, output = collatz_eval((b, e), lc)
	return output

def generate_list(interval: int, num_intervals: int) -> List[int]:
	LCACHE_SIZE = 1000001
	lc = [0] * LCACHE_SIZE
	out = [0] * num_intervals
	for i in range(0, num_intervals):
		out[i] = max_cl_range((i*interval)+1, (i+1)*interval, lc)
	return out

def print_list(lc: List[int]) -> None:
	f=open("mc.txt", "a")
	f.write("[")
	for i in lc:
		f.write(str(i) + ",")
	f.write("]")
	f.close()

def generate_input() -> None:
	f=open("RunCollatzInTmp.txt", "a")
	l = int(random() * 999998) + 1
	r = int(random() * 999998) + 1
	f.write(str(l) + " " + str(r))
	for x in range(250):
		l = int(random() * 999998) + 1
		r = int(random() * 999998) + 1
		f.write("\n" + str(l) + " " + str(r))

		
if __name__ == "__main__" :
	generate_input()